<h1 align="center"> DripDrop </h1> 
<h3 align="center"> Making Water accessible to all </h3>



## Features 📚

✔️ Login/SignUp\
✔️ Carbon Emission Calculator & Tracker\
✔️ Clean Water Locations on map near you\
✔️ Daily Activities\
✔️ Water Saving Tracker\
✔️ Community\
✔️ Water Donation\

## Getting Started....
- The app is completely built on `flutter` framework of `dart` and that's why we need `flutter sdk` and much more do your flutter setup by going to the folloewing link
- https://docs.flutter.dev/get-started/install/windows
-  , after the successful of setup installation, clone the repository into your local system using below command:

```bash
# Clone this repository
$ git clone https://gitlab.com/Mughees_/drop-saver

# Go into the repository
$ cd drop-saver

# Open project in VS code
$ code .

# Install dependencies by going to pubspec.yaml
$ flutter pub get

# To run the project
$ flutter run

```


# This is it , next simply start emulator and run this app :)

## and if you are having any trouble in ruuning this app just email us and we'll help you ASAP..!

```
# mr.mugy4@gmail.com

```
